/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package tarea.pkg1;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 *
 * @author PC
 */
public class Tarea1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner sc = new Scanner(System.in);
        int c = 0, a = 0, i = 0, n = 0;
        boolean repetir;
        do {
            repetir = false;
            try {
                System.out.print("INSERTAR PRIMER NÚMERO ENTERO: ");
                c = sc.nextInt();

                System.out.print("INSERTAR SEGUNDO NUMERO ENTERO: ");
                a = sc.nextInt();

                System.out.print("INSERTAR TERCER NUMERO ENTERO: ");
                i = sc.nextInt();

                System.out.print("INSERTAR CUARTO NUMERO ENTERO: ");
                n = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("VALOR NO VALIDO" + e.toString());
                sc.nextLine();
                repetir = true;
            }
        } while (repetir);
        System.out.println("int INTRODUCIDO -> " + c);
        System.out.println("int INTRODUCIDO -> " + a);
        System.out.println("int INTRODUCIDO -> " + i);
        System.out.println("int INTRODUCIDO -> " + n);
    }
}
